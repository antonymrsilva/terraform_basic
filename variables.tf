variable "aws_region" {
  type        = string
  description = ""
  default     = "us-east-2"
}

variable "instance_ami" {
  type        = string
  description = ""
  default     = "ami-0629230e074c580f2"

}

variable "instance_type" {
  type        = string
  description = ""
  default     = "t2.micro"
}

variable "instance_tags" {
  type        = map(string)
  description = ""
  default = {
    Name    = "Ubuntu"
    Project = "Teste Terraform"
  }
}